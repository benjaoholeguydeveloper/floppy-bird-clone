FROM node:16.13.2
WORKDIR /usr/src/flappy-bird-clone
COPY package*.json ./
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install
COPY . .
EXPOSE 8080
CMD ["npm", "run", "dev"]