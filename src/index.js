
import Phaser from "phaser";
import PlayScene from './scenes/PlayScene';
import MenuScene from './scenes/MenuScene';
import PreloadScene from './scenes/PreloadScene';
import ScoreScene from './scenes/ScoreScene';
import PauseScene from './scenes/PauseScene';



const WIDTH = 400;
const HEIGHT = 600;
const BIRD_POSITION = {x: WIDTH * 0.1, y: HEIGHT/2};

const SHARED_CONFIG = {
    width: WIDTH,
    height: HEIGHT,
    startPosition: BIRD_POSITION 
};

const Scenes = [PreloadScene, MenuScene, ScoreScene, PlayScene, PauseScene];
const createScene = (Scene)=>new Scene(SHARED_CONFIG)
const initScenes = () => Scenes.map(createScene)

const config = {
    // WebGL (web graphics library). JS Api for rendering 2D and 3D graphics. It is the renderer 
    type: Phaser.AUTO,
    ...SHARED_CONFIG,
    pixelArt: true,
    physics: {
        // Arcade physics plugin, manages physic simulation
        default: 'arcade',
        arcade: {
            // debug: true,
        }
    },
    // scene: [PreloadScene, new MenuScene(SHARED_CONFIG), new PlayScene(SHARED_CONFIG)]
    scene: initScenes()
};





const VELOCITY = 200;


let bird = null;
let pipes = null;
// let upperPipe = null;
// let lowerPipe = null;



const initialBirdPosition = {x: config.width * 0.1, y:config.height/2}


// Loading assets, such as images, music, animations
function preload () {
  


  // 'this' context - scene 
  // contains functions and properties we can use
  // debugger;
}
// Initializing instances of the objects in the memory
function create () {

  

  

}

// 60 fps
// 60 times per second
function update(time, delta){

}



  

new Phaser.Game(config);