import BaseScene from './BaseScene';

const PIPES_TO_RENDER = 4;


class PlayScene extends BaseScene {
    constructor(config){
        super('PlayScene',config);
        this.bird = null;
        this.pipes = null;
        this.isPaused = false;

        this.pipeHorizontalDistance = 0;

        // this.pipeVerticalDistanceRange = [150, 250];
        // this.pipeHorizontalDistanceRange = [300, 350];

        this.flapVelocity = 300;

        this.score = 0;
        this.scoreText = '';

        this.currentDifficulty = 'easy';
        this.difficulties = {
            'easy':{
                pipeHorizontalDistanceRange : [400, 550],
                pipeVerticalDistanceRange : [350, 400]
            },
            'normal':{
                pipeHorizontalDistanceRange : [300, 380],
                pipeVerticalDistanceRange : [250, 310]
            },
            'hard':{
                pipeHorizontalDistanceRange : [400, 550],
                pipeVerticalDistanceRange : [120, 170]
            }
        }
    }


    create(){
        this.currentDifficulty = 'easy';
        super.create();
        this.createBird();
        this.createPipes();
        this.createColliders();
        this.createScore();
        this.createPause();
        this.handleInputs();
        this.listenToEvents();
        this.fly();
        
    }

    fly(){
        this.anims.create({
            key: 'fly',
            frames: this.anims.generateFrameNumbers('bird',{start:8, end: 15}),
            // 24 fps default, it will play animation consisting of 24 frames in 1 second
            // in case of framerate 2 and sprite of 8 frames animations will play in 4 sec; 8/2
            frameRate: 16,
            // repeat infinitelly
            repeat: -1
        })

        this.bird.play('fly');
    }

    flap(){
        if(this.isPaused){return;}

        this.bird.body.velocity.y = -this.flapVelocity;
    }
      
    placePipe(uPipe, lPipe){
        const difficulty = this.difficulties[this.currentDifficulty];


        const rightMostX = this.getRightMostPipe();
      
        const pipeVerticalDistance = Phaser.Math.Between(...difficulty.pipeVerticalDistanceRange);
        const pipeVerticalPosition = Phaser.Math.Between(0 + 20, this.config.height - 20 - pipeVerticalDistance);
      
        const pipeHorizontalDistance = Phaser.Math.Between(...difficulty.pipeHorizontalDistanceRange);
      
        uPipe.x = rightMostX + pipeHorizontalDistance;
        uPipe.y = pipeVerticalPosition;
      
        lPipe.x = uPipe.x;
        lPipe.y = uPipe.y + pipeVerticalDistance;
      
    }

    recyclePipes(){
        const tempPipes = [];
        this.pipes.getChildren().forEach(pipe=>{
            if(pipe.getBounds().right <= 0){
                // recycle pipe
                // get here upper and lower that are out of the bound
                tempPipes.push(pipe);
                if(tempPipes.length === 2){
                    this.placePipe(...tempPipes);
                    this.increaseScore();
                    this.saveBestScore();

                    this.increaseDifficulty();
                }
        
            }
        })
    }

    increaseDifficulty(){
        if(this.score === 5){
            this.currentDifficulty = 'normal';
        }
        if(this.score === 10){
            this.currentDifficulty = 'hard';
        }
    }

    saveBestScore(){
        const bestScoreText = localStorage.getItem('bestScore');
        const bestScore = bestScoreText && parseInt(bestScoreText,10);

        if(!bestScore || this.score > bestScore){
            localStorage.setItem('bestScore',this.score);
        }
    }

    gameOver(){
        // this.bird.x = this.config.startPosition.x;
        // this.bird.y = this.config.startPosition.y;
        // this.bird.body.velocity.y = 0;

        // this.physics.pause();

        this.pipes.setVelocity();

        this.saveBestScore();

        this.die();

        this.time.addEvent({
            delay: 3000,
            callback: ()=>{
                this.scene.restart();
            },
            loop: false,
        })
    } 
      
    getRightMostPipe(){
        let rightMostX = 0;
      
        this.pipes.getChildren().forEach(function(pipe){
            rightMostX = Math.max(pipe.x, rightMostX);
        });
        return rightMostX;
    }   

    update(){

        this.checkGameStatus();
    
        this.recyclePipes();
    }

    listenToEvents(){
        if(this.pauseEvent){return;}
        this.pauseEvent = this.events.on('resume',()=>{
            this.initialTime = 3;
            this.countDownText = this.add.text(...this.screenCenter, `Fly in: ${this.initialTime}`,this.fontOptions).setOrigin(0.5);
            this.timedEvent = this.time.addEvent({
                delay: 1000,
                callback: this.countDown,
                callbackScope: this,
                loop: true
            })
        })
    }

    countDown(){
        this.initialTime--;
        this.countDownText.setText(`Fly in: ${this.initialTime}`)
        if(this.initialTime <= 0){
            this.isPaused = false;
            this.countDownText.setText('');
            this.physics.resume();
            this.timedEvent.remove();
        }
    }

    checkGameStatus(){
        if (this.bird.getBounds().bottom >= this.config.height || this.bird.y <= 0) {
            // alert("you have lost");
            this.gameOver();
        }
    }

    createBG(){
        this.add.image(0, 0, 'sky').setOrigin(0);

    }

    createPipes(){
        this.pipes = this.physics.add.group();
  
        for (let i = 0; i < PIPES_TO_RENDER; i++){
            
            const upperPipe = this.pipes.create(0, 0, 'pipe')
                .setImmovable(true)
                .setOrigin(0,1);
            const lowerPipe = this.pipes.create(0, 0, 'pipe')
                .setImmovable(true)
                .setOrigin(0,0);

            this.placePipe(upperPipe, lowerPipe);
        
        }

        this.pipes.setVelocityX(-200);
    }

    createColliders(){
        this.physics.add.collider(this.bird, this.pipes, this.gameOver, null, this);
    }

    createScore(){
        this.score = 0;
        const bestScore = localStorage.getItem('bestScore');
        this.scoreText = this.add.text(16,16,`Score: ${0}`,{fontSize:'32px',fill:'#000'});
        this.add.text(16,52,`Best score: ${bestScore || 0}`,{fontSize:'18px',fill:'#000'});
    }

    createPause(){
        this.isPaused = false;

        const pauseButton = this.add.image(this.config.width - 10, this.config.height - 10, 'pause')
        .setInteractive()
        .setScale(3)
        .setOrigin(1);

        pauseButton.on('pointerdown', ()=>{
            this.isPaused = true;

            this.physics.pause();
            this.scene.pause();
            this.scene.launch('PauseScene');
        })
    }

    createBird(){
        // middle of the height, 1/10 width
        this.bird = this.physics.add.sprite(this.config.startPosition.x, this.config.startPosition.y, 'bird')
        .setFlipX(true)
        .setScale(3)
        .setOrigin(0);

        this.bird.setBodySize(this.bird.width, this.bird.height-7);
        this.bird.body.gravity.y = 600;
        this.bird.setCollideWorldBounds(true);
    }

    die(){

        this.countDownText = this.add.text(...this.screenCenter, 'Game Over',this.fontOptions).setOrigin(0.5);

        this.anims.create({
            key: 'die',
            frames: this.anims.generateFrameNumbers('bird',{start:16, end: 18}),
            // 24 fps default, it will play animation consisting of 24 frames in 1 second
            // in case of framerate 2 and sprite of 8 frames animations will play in 4 sec; 8/2
            frameRate: 2,
            repeat: 0
        })

        this.bird.setTint(0xCAC6C6);

        this.removeInputs();

        this.bird.play('die');
        this.bird.setFlipY(true)
        
    }

    handleInputs(){
        // physer keys capture event

        this.input.on('pointerdown',this.flap, this);
        this.input.keyboard.on('keydown_SPACE', this.flap, this)
    }

    removeInputs(){
        this.input.off('pointerdown');
        this.input.keyboard.off('keydown_SPACE')
    }

    increaseScore(){
        this.score++;
        this.scoreText.setText(`Score: ${this.score}`);
    }

}

export default PlayScene;